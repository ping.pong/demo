package Utilities;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Retryclass implements IRetryAnalyzer {

	int count = 0;
	int retrycount = 0;

	public boolean retry(ITestResult result) {

		if (count < retrycount) {
			count++;
			return true;
		}
		return false;
	}

}
