package Utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
//import org.apache.commons.io.FileUtils;

import Base.Baseclass;

public class Listeners extends Utilities implements ITestListener {

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onTestSuccess(ITestResult result) {

		System.out.println("passed");
		// TODO Auto-generated method stub

	}

	public void onTestFailure(ITestResult result) {

		System.out.println("test has been failed please refer to the screenshot in \"src//test//java\" folder");

		TakesScreenshot ts = (TakesScreenshot) driver;

		File src = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File("src\\test\\java\\" + "Mahesh_New"+ "1" + ".png" ));
	/*		FileUtils.copyFile(src, new File("src\\test\\java\\" + "Mahesh_New"+ "2"+ ".png" ));
			FileUtils.copyFile(src, new File("src\\test\\java\\" + "Mahesh_New"+ "3"+ ".png" ));
			FileUtils.copyFile(src, new File("src\\test\\java\\" + "Mahesh_New"+ "4" + ".png" ));*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TODO Auto-generated method stub

	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub

	}

}
