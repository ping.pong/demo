package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import CommonPageobjectpackage.CommonPageobjects;

public class Utilities {

	protected static WebDriver driver;

	protected Properties p;

	String url, email, password;

	Object[][] o = new Object[4][6];

	public void properties_data() throws IOException {
		p = new Properties();

		FileInputStream f = new FileInputStream("src\\test\\java\\Data");

		p.load(f);

	}

}
