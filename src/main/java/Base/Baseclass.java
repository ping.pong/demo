package Base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import CommonPageobjectpackage.CommonPageobjects;
import Pages_package.Loggedinhomepage;
import Pages_package.LoggedoutHomepage;

public class Baseclass {

	public WebDriver driver;

	@Parameters({ "browsername" })
	@BeforeMethod
	public void initialization(String browsername) {

		System.out.println(browsername);

		if (browsername.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", "src\\test\\java\\chromedriver.exe");

			driver = new ChromeDriver();

		}

		else if (browsername.equals("firefox")) {

			System.setProperty("webdriver.gecko.driver", "src\\test\\java\\geckodriver.exe");
			driver = new FirefoxDriver();
		}

		else {
			System.out.println("different browser");
		}

	}
	
}
