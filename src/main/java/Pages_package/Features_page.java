package Pages_package;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Base.Baseclass;
import Utilities.Utilities;

public class Features_page extends Utilities {

	/*
	 * @FindBy(xpath = "//div[@class='col-md-4 col-md-offset-4']//a") private
	 * WebElement getstarted_button;
	 */

/*	@FindBy(xpath = "//div[@id='start-button']//div//child::a[contains(text(), 'Get started now')]")
	private WebElement getstarted_button;*/

	/*
	 * @FindBy(xpath = "//div[@id='cloud-store']//figure//img") private WebElement
	 * opencartcloud_img;
	 */

	@FindBy(xpath = "(//div[@id='cms-feature']//div[@class='page-header']//div//h1)[1]")
	private WebElement opencart_features_text;

	public Features_page(WebDriver driver) {

		super.driver = driver;

		PageFactory.initElements(driver, this);

	}

	public void verifying_opencart_features_text() {

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.visibilityOf(opencart_features_text));

		Assert.assertEquals(opencart_features_text.getText(), "OpenCart Features");
		
		//Assert.assertEquals(opencart_features_text.getText(), "OpenCar");

	}

}
