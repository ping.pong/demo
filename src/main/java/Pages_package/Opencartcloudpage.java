package Pages_package;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Utilities.Utilities;

public class Opencartcloudpage extends Utilities{
	
	
	
	@FindBy(xpath = "//div[@id='start-button']//div//child::a[contains(text(), 'Get started now')]")
	private WebElement getstarted_button;
	
	
	public Opencartcloudpage(WebDriver driver) {
		
		super.driver=driver;
		PageFactory.initElements(driver, this);
	}
	

	public void verify_getstarted_button() {
		
		Assert.assertTrue(getstarted_button.isDisplayed(), "get started button is not displayed");
		
		//Assert.assertTrue(!getstarted_button.isDisplayed(), "get started button is not displayed");
	}
	

}
