package Pages_package;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Utilities.Utilities;

public class Loggedooutlandingpage extends Utilities {

	// String[][] ob = new String[3][6];

	@FindBy(id = "input-email")
	private WebElement email;

	@FindBy(id = "input-password")
	private WebElement password;

	@FindBy(xpath = "//div//button[contains(text(), 'Login')][1]")
	private WebElement Login_button;

	@FindBy(id = "input-pin")
	private WebElement pin;

	@FindBy(xpath = "//div//button[@class='btn btn-primary btn-lg']")
	private WebElement continue_button;

	@FindBy(css = "input[name='username']")
	private WebElement user_name;

	@FindBy(id = "input-firstname")
	private WebElement first_name;

	@FindBy(id = "input-lastname")
	private WebElement last_name;

	public Loggedooutlandingpage(WebDriver driver) {

		super.driver = driver;

		PageFactory.initElements(driver, this);

	}

	public void signing_in() throws IOException {

		properties_data();

		String mailaddress = p.getProperty("email");

		String pswrd = p.getProperty("password");

		String pin_number = p.getProperty("pin");

		email.sendKeys(mailaddress);

		password.sendKeys(pswrd);
		Login_button.click();

		if (pin.isDisplayed()) {
			pin.sendKeys(pin_number);
			continue_button.click();
		}

	}

	public void Entering_data(String username, String password) {

		user_name.sendKeys(username);

		System.out.println("done");

	}

}
