package Pages_package;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Base.Baseclass;
import Utilities.Utilities;

public class Loggedinhomepage extends Utilities {

	// private WebDriver driver;

	@FindBy(xpath = "//div[@class='page-header']//div//following-sibling::p")
	private WebElement welcome_text;

	@FindBy(xpath = "//div[@id='navbar-collapse-header']//ul//li//a[contains(text(), 'Features')]")
	private WebElement features;

	@FindBy(css = "a[data-toggle='dropdown']")
	private WebElement Resources;

	@FindBy(xpath = "//ul//li//a[@data-toggle='dropdown']//following-sibling::ul//a[contains(text(),'OpenCart Cloud')]")
	private WebElement opencart_cloud;

	public Loggedinhomepage(WebDriver driver) {
		super.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void verifying_welcomepage() {

		 Assert.assertTrue(welcome_text.isDisplayed(), "user account is not being logged in");

		//Assert.assertTrue(!welcome_text.isDisplayed(), "user account is not being");

	}

	public Opencartcloudpage Navigating_to_opencart_cloud() {
		Resources.click();
		opencart_cloud.click();
		return new Opencartcloudpage(driver);

	}

	public Features_page clickingresouces_and_selecting_features() {
		features.click();

		return new Features_page(driver);

	}

}
