package Pages_package;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Base.Baseclass;
import Utilities.Utilities;

public class LoggedoutHomepage extends Utilities {

	@FindBy(xpath = "(//div[@class='navbar-right hidden-xs']//a)[1]")
	private WebElement loginbutton;

	@FindBy(css = "a[class='btn btn-black navbar-btn']")
	private WebElement Register;
	

	@FindBy(className = "btn btn-link navbar-btn")
	private WebElement Account;

	public LoggedoutHomepage(WebDriver driver) {

		super.driver = driver;

		PageFactory.initElements(driver, this);

	}

	public void launchurl() throws IOException {
		properties_data();
		System.out.println("launching url");
		String url = p.getProperty("url");
		driver.manage().window().maximize();
		driver.get(url);
	}
	
	
	public void waiting() {
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public void clicking_on_login_button() {

		//WebDriverWait wait = new WebDriverWait(driver, 5);

		//wait.until(ExpectedConditions.visibilityOf(loginbutton));

		if(loginbutton.isDisplayed()) {
		loginbutton.click();
		}
		
		//wait.until(ExpectedConditions.visibilityOf(Account));
		else{
			
			Account.click();
		}
	}

	public void clicking_on_register_button() {
		Register.click();

	}
	
	
	

}
