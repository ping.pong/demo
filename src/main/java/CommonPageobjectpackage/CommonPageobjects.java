package CommonPageobjectpackage;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import Base.Baseclass;
import Pages_package.Loggedinhomepage;
import Pages_package.Loggedooutlandingpage;
import Pages_package.LoggedoutHomepage;
import Utilities.Listeners;
import Utilities.Utilities;

public class CommonPageobjects extends Baseclass {

	protected LoggedoutHomepage loggedouthomepage;
	protected Loggedinhomepage loggedinhomepage;
	protected Loggedooutlandingpage loggedoutlandingpage;


	@BeforeMethod
	public void commonobjectsinitialization() {
		loggedouthomepage = new LoggedoutHomepage(driver);
		loggedinhomepage = new Loggedinhomepage(driver);
		loggedoutlandingpage = new Loggedooutlandingpage(driver);

		//listeners = new Listeners(driver);

	}

}
