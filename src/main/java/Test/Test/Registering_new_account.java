package Test.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import CommonPageobjectpackage.CommonPageobjects;
import Pages_package.Loggedooutlandingpage;

public class Registering_new_account extends CommonPageobjects {

	Object[][] o = new Object[3][6];

	public Object[][] datafetch() throws IOException {

		File f = new File("src\\test\\java\\TestData.xlsx");

		FileInputStream inputStream = new FileInputStream(f);

		XSSFWorkbook H = new XSSFWorkbook(inputStream);

		int numberOfSheets = H.getNumberOfSheets();

		for (int k = 0; k < numberOfSheets; k++) {

			if (H.getSheetName(k).equalsIgnoreCase("TestData")) {

				XSSFSheet sheet = H.getSheetAt(k);

				Iterator<Row> rows = sheet.iterator();

				// Row firstrow = rows.next();

				while (rows.hasNext()) {

					Row next_row = rows.next();

					Iterator<Cell> cells = next_row.cellIterator();

					int i = 0;

					int j = 0;

					while (cells.hasNext()) {

						Cell cell_value = cells.next();

						o[i][j] = cell_value.getStringCellValue();

						// System.out.println(o[i][j]);

						j++;

					}

					i++;
				}

			}

		}
		return o;

	}

	@DataProvider(name = "data_provider")
	public Object[][] registering() throws IOException {

		Object ob[][] = datafetch();

		return ob;
	}

	@Test(dataProvider = "data_provider")
	public void Registering_new_account(String username, String firstname, String lastname, String Email,
			String country, String password) throws IOException {

		loggedouthomepage.launchurl();
		loggedouthomepage.clicking_on_register_button();
		// loggedoutlandingpage.Entering_data(username, password);

		WebElement user_name = driver.findElement(By.cssSelector("input[name='username']"));
		/*
		 * @FindBy(css = "input[name='username']") private WebElement user_name;
		 */

		user_name.sendKeys(username);
		System.out.println(username);
		// Object[][] data = loggedoutlandingpage.ab();

	}

	@AfterMethod
	public void close() {
		driver.close();
		Registering_new_account ob = new Registering_new_account();

		Class c = ob.getClass();

		System.out.println(c.getName() + " has ended");

	}

}