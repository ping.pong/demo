package Test.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import Base.Baseclass;
import CommonPageobjectpackage.CommonPageobjects;
import Pages_package.Features_page;
import Utilities.Listeners;
import Utilities.Retryclass;

@org.testng.annotations.Listeners(Utilities.Listeners.class)

public class Features_verifications_case extends CommonPageobjects {

	Features_page featurespage;

	@Test(retryAnalyzer = Utilities.Retryclass.class)
	public void Features_verifications_case() throws IOException {

		loggedouthomepage.launchurl();
		loggedouthomepage.clicking_on_login_button();
		loggedoutlandingpage.signing_in();
		featurespage = loggedinhomepage.clickingresouces_and_selecting_features();

		featurespage.verifying_opencart_features_text();

	}

	@AfterMethod
	public void closing() {

		driver.close();

		Features_verifications_case ob = new Features_verifications_case();

		Class c = ob.getClass();

		System.out.println(c.getName() + " has ended");

	}

}
