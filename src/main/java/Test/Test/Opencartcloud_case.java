package Test.Test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import CommonPageobjectpackage.CommonPageobjects;
import Pages_package.Opencartcloudpage;


public class Opencartcloud_case extends CommonPageobjects {

	Opencartcloudpage opencartcloudpage;

	@Test(retryAnalyzer = Utilities.Retryclass.class)
	public void Opencartcloud_case() throws IOException {

		loggedouthomepage.launchurl();
		loggedouthomepage.waiting();
		loggedouthomepage.clicking_on_login_button();
		loggedoutlandingpage.signing_in();
		opencartcloudpage = loggedinhomepage.Navigating_to_opencart_cloud();

		opencartcloudpage.verify_getstarted_button();

	}

	@AfterMethod
	public void closing() {
		driver.close();

		Opencartcloud_case ob = new Opencartcloud_case();

		Class c = ob.getClass();

		System.out.println(c.getName() + " has ended");
		
	}

}
