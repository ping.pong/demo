package Test.Test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.Baseclass;
import CommonPageobjectpackage.CommonPageobjects;
import Pages_package.Accountdetailspage;
import Pages_package.LoggedoutHomepage;
import Pages_package.Features_page;

public class Loggingin_case extends CommonPageobjects {

	@Test(retryAnalyzer = Utilities.Retryclass.class)
	public void Loggingin_case() throws IOException {

		loggedouthomepage.launchurl();

		loggedouthomepage.clicking_on_login_button();

		loggedoutlandingpage.signing_in();

		loggedinhomepage.verifying_welcomepage();

	}

	@AfterMethod
	public void closing() {
		driver.close();
		
		Loggingin_case ob = new Loggingin_case();

		Class c = ob.getClass();

		System.out.println(c.getName() + " has ended");
	}

}
